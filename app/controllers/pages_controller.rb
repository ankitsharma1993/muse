class PagesController < ApplicationController
	
	before_action :authenticate_user!, except: [:index]

	def search
		artist=params["search"]
		user_id=current_user.id
		dup_count=SearchedArtist.where(artist_name: artist).count
		if dup_count==0
			new_row=SearchedArtist.create user_id: user_id, artist_name: artist
		end
		@artist_info, @top_tracks, @error_info=SearchedArtist.getArtistInfo(artist)
		@error,@similar_artists=SearchedArtist.getSimilarArtists(artist)
		render 'user_home'
	end 

	def search_history
		@searched_artists=SearchedArtist.getSearchedArtists(current_user.id)
		render 'user_home'
	end
end