class SearchedArtist < ActiveRecord::Base


	def self.getArtistInfo(artist)
		artist_info_req=HTTParty.get("http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=#{artist}&api_key=a94b06e62d0e1cfecfe3010e5cc0b790&format=json")
		top_tracks_req=HTTParty.get("http://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&artist=#{artist}&api_key=a94b06e62d0e1cfecfe3010e5cc0b790&format=json")
		artist_info={}
		if artist_info_req.parsed_response["error"].nil?	
			artist_info["name"]=artist_info_req.parsed_response["artist"]["name"]
			artist_info["url"]=artist_info_req.parsed_response["artist"]["url"]
			artist_info["bio"]=artist_info_req.parsed_response["artist"]["bio"]["summary"]
			top_tracks=[]
			top_tracks_req.parsed_response["toptracks"]["track"].each do |track|
				tracks={}
				tracks["name"]=track["name"]
				tracks["url"]=track["url"]
				top_tracks<<tracks
			end
		else
			error_info=artists_info_req.parsed_response["message"]
		end
		return artist_info, top_tracks, error_info
	end

	def self.getSimilarArtists(artist)
		similar_artists_array=[]
		similar_artists_req=HTTParty.get("http://ws.audioscrobbler.com/2.0/?method=artist.getSimilar&artist=#{artist}&api_key=a94b06e62d0e1cfecfe3010e5cc0b790&format=json")
		if similar_artists_req.parsed_response["error"].present?
			error=similar_artists_req.parsed_response["message"]
		elsif similar_artists_req.parsed_response["similarartists"]["artist"].kind_of?(Array)
			similar_artists_req.parsed_response["similarartists"]["artist"].each do |artist|
				similar_artists={}
				similar_artists["name"]=artist["name"]
				similar_artists["url"]=artist["url"]
				similar_artists_array<<similar_artists
			end
		else
			error="No Results Found."
		end
		return error,similar_artists_array
	end

	def self.getSearchedArtists(id)
		searched_artists=[]
		temp=SearchedArtist.where(user_id: id)
		temp.each do |artist|
			searched_artists<<artist.artist_name
		end
		searched_artists
	end
end
